<?php

/**
 * @file
 * Description
 */

 /**
  * This sniff prohibits the use of Perl style hash comments.
  *
  * PHP version 8
  *
  * @category PHP
  * @package  PHP_CodeSniffer
  * @author   Your Name <you@domain.net>
  * @license  http://matrix.squiz.net/developer/tools/php_cs/licence BSD Licence
  * @version  SVN: $Id: coding-standard-tutorial.xml,v 1.9 2008-10-09 15:16:47 cweiske Exp $
  * @link     http://pear.php.net/package/PHP_CodeSniffer
  */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/* The Comment model has a one-to-many inverse relation with the User model, a one-to-many inverse
relation with the Post model, and a one-to-many relation with the Reply model */
class Comment extends Model
{
    use HasFactory;

    /* table name to be used by model */
    protected $table = 'comments';

    /* columns to be allowed in mass-assingment */
    protected $fillable = ['user_id', 'post_id', 'body'];

    /**
     * One-to-Many inverse relation with User model.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * One-to-Many inverse relation with Post model.
     */
    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    /**
     * One-to-Many relation with Reply model.
     */

    public function replies()
    {
            return $this->hasMany(Reply::class, 'comment_id');
    }
}
